De facut in Developer:
	Structuri Developer: (respecta exact numele block_index, comp_vals, ofst, gain)
		1. Updated_Comp_Vals_s_t: dataType updated_comp_vals
			- updated_comp_vals are 1xblock_index si 1xcomp_vals            
			- comp_vals are 1xofst si 1xgain

		2. Stored_Comp_Vals_s_t: dataType array de 20xcomp_vals
			- comp_vals are 1xofst si 1xgain
	Schimba declaratiile structurilor:
		1. stored_comp_val_t, updated_comp_val_t in appdiag_Swc_autosar_inf.c
		2. updatedCompVals, nvmCompVals, pNvmFrameworkActive in PERSISTENT_CDD.c


De facut in PERSISTENT_CDD:
	1. compValsWriteRequestFlag trebuie declarat ca variabila globala statica 
		- poate pot face o structura pentru flags in general si asta sa fie primul
		- adauga comentariu ca trebuie updatat pentru fiecare tip nou de structura
	2. un vector NvBlockIDMapping[20] ca static pentru maparea index -> block id
		- pentru functia mapAppdiadIndexToNvmId
		- comentariu ca trebuie updatat pentru fiecare nvblock nou
	3. blockIdCompVals trebuie declarat ca variabila globala statica
		- poate pot face o structura pentru blockIDs in general si asta sa fie primul
		- adauga comentariu ca trebuie updatat pentru fiecare tip nou de structura


SS-uri de facut pentru testare:
	1. E chemat DID-ul 
	2. Ce citesc in PERSISTENT_CDD (rte_read, ss) scriu inapoi in APPDIAG (rte_read, ss)
	3. SS pentru Persistent_CDD_INIT (scriu un nvblock, las ECU-ul sa mearga in sleep, il trezesc, citesc in init)


De facut modificari in .h-uri (PersistentData, PersistentCDD, appdiag_autosar, INF_CDD): 
	1. Parametrii functiilor Rte_Read si Rte_Write, numele structurilor


De facut modificari in codul sursa:
Verifica ca blocurile din tichet au toate activat ReadAll
Verifca ca nu mai exista define-uri de genul "NvM_Did_FD27_cell14" in appdiag_swc_autosar_inf.c
Redu ciclicitatea functiei main a Persistent CDD la 10ms (acum e 20ms)
Verifica cum ai definit indexii pentru nvblocks in PersistentData.h (e folosit de INF_CDD in mapHwioChToNvmCh)
Modifica functia NvM_GetVal din INF_CDD.c


Comentarii modificari de adaugat in tichet pentru commit:
	- defined indexes for every nvblock in appdiag_swc_autosar_inf.c
	- modified the read and write functions for the DIDS in appdiag_swc_autosar_inf.c to in order to correctly use the new
				rte interfaces 
	- wrote to appdiag the values obtained from nvm_ReadAll in the persistent_cdd_init function through the rte interface
	- added in the persistent cdd main function new code which writes to the nvblocks the data from the DIDs in the ticket
	- increased the priorities for the nvblocks in  this ticket
	- reduced the cycle time of the Persistent CDD main function (from 20ms to 10ms)
	- removed the CORESTORE_Comp_Ch_t enumeration from appdiag_swc_autosar_inf.c and PersistentData.h; 
	- modified the stored_comp_vals_s_port, updated_comp_vals_s_port interfaces;
	- modified mapHwioChToNvmCh by commenting out the HWIO_AIN_CH__12V_SPLY case (reason: no DID for reading/writing the in the NvM)
	- modified the parameters of mapHwioChToNvmCh, instead of a pointer to the nvm channel now it expects the ID of the NvBlock
	- modified the APPDIAG_ClrComp function from appdiag_swc_autosar_inf to work with the new RTE interfaces
	- created the function mapAppdiadIndexToNvmId to obtain the block id for a nvblock using the index received from APPDIAG\
	- renamed CORESTORE_GetCmpstnVal to NvM_GetVal because it will now work with the nvblocks




0xFD14: APPDIAG_ReadStackComp, APPDIAG_WriteStackComp; 
0xFD15: APPDIAG_ReadPostComp, APPDIAG_WritePostComp 
0xFD16: APPDIAG_ReadCurrInstComp, APPDIAG_WriteCurrInstComp 
0xFD17: APPDIAG_ReadTemp1Comp, APPDIAG_WriteTemp1Comp 
0xFD18: APPDIAG_ReadTemp2Comp, APPDIAG_WriteTemp2Comp 
0xFD19: APPDIAG_ReadTemp3Comp, APPDIAG_WriteTemp3Comp 
0xFD1A: APPDIAG_ReadCell01Comp, APPDIAG_WriteCell01Comp 
0xFD1B: APPDIAG_ReadCell02Comp, APPDIAG_WriteCell02Comp 
0xFD1C: APPDIAG_ReadCell03Comp, APPDIAG_WriteCell03Comp 
0xFD1D: APPDIAG_ReadCell04Comp, APPDIAG_WriteCell04Comp
0xFD1E: APPDIAG_ReadCell05Comp, APPDIAG_WriteCell05Comp
0xFD1F: APPDIAG_ReadCell06Comp, APPDIAG_WriteCell06Comp
0xFD20: APPDIAG_ReadCell07Comp, APPDIAG_WriteCell07Comp
0xFD21: APPDIAG_ReadCell08Comp, APPDIAG_WriteCell08Comp
0xFD22: APPDIAG_ReadCell09Comp, APPDIAG_WriteCell09Comp
0xFD23: APPDIAG_ReadCell10Comp, APPDIAG_WriteCell10Comp
0xFD24: APPDIAG_ReadCell11Comp, APPDIAG_WriteCell11Comp
0xFD25: APPDIAG_ReadCell12Comp, APPDIAG_WriteCell12Comp
0xFD26: APPDIAG_ReadCell13Comp, APPDIAG_WriteCell13Comp
0xFD27: APPDIAG_ReadCell14Comp, APPDIAG_WriteCell14Comp


Aux_Comp_Vals_t     *nvmCompValsFD14 = 0;
Aux_Comp_Vals_t     *nvmCompValsFD15 = 0;
Aux_Comp_Vals_t     *nvmCompValsFD16 = 0;
Aux_Comp_Vals_t     *nvmCompValsFD17 = 0;
Aux_Comp_Vals_t     *nvmCompValsFD18 = 0;
Aux_Comp_Vals_t     *nvmCompValsFD19 = 0;
Aux_Comp_Vals_t     *nvmCompValsFD1A = 0;
Aux_Comp_Vals_t     *nvmCompValsFD1B = 0;
Aux_Comp_Vals_t     *nvmCompValsFD1C = 0;
Aux_Comp_Vals_t     *nvmCompValsFD1D = 0;
Aux_Comp_Vals_t     *nvmCompValsFD1E = 0;
Aux_Comp_Vals_t     *nvmCompValsFD1F = 0;
Aux_Comp_Vals_t     *nvmCompValsFD20 = 0;
Aux_Comp_Vals_t     *nvmCompValsFD21 = 0;
Aux_Comp_Vals_t     *nvmCompValsFD22 = 0;
Aux_Comp_Vals_t     *nvmCompValsFD23 = 0;
Aux_Comp_Vals_t     *nvmCompValsFD24 = 0;
Aux_Comp_Vals_t     *nvmCompValsFD25 = 0;
Aux_Comp_Vals_t     *nvmCompValsFD26 = 0;
Aux_Comp_Vals_t     *nvmCompValsFD27 = 0;
